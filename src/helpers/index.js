/**
 * @FP
 */
const join = function (...functions) {
  return function (...args) {
    for (let i = 0; i < functions.length; i += 1) {
      functions[i](...args);
    }
  };
};

/**
 * @MISC
 */
const rightPick = function (...pickedKeys) {
  return function (object) {
    return Object.keys(object).filter(key => pickedKeys.includes(key))
      .reduce((acc, curr) => {
        acc[curr] = object[curr];
        return acc;
      }, {});
  };
};

const leftPick = function (...pickedKeys) {
  return function (object) {
    return pickedKeys.reduce((acc, curr) => {
      acc[curr] = object[curr];
      return acc;
    }, {});
  };
};

function compareArrayOfTuples(anArray, anotherArray) { 
  if (anArray.some(key => !(key in anotherArray) || anArray[key] !== anotherArray[key])) {
    return false;
  }
  if (anotherArray.some(key => !(key in anArray))) {
    return false;
  }
  return true;
}

function compareTwoObjects(aObject, anotherObject) {
  return compareArrayOfTuples(Object.keys(aObject), Object.keys(anotherObject));
}

// Assuming the invariant that both objects have those keys
function equalityOnKeys(...keysToCompare) {
  return function (aObject, anotherObject) {
    return keysToCompare.every(key => aObject[key] === anotherObject[key]);
  };
}

export { join, leftPick, rightPick, equalityOnKeys, compareArrayOfTuples, compareTwoObjects };
export default join;
